from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from Authentication.views import *
from Authentication.apps import *
import unittest

# Create your tests here.
class UnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 302)
    def test_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)
    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    def test_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
    def test_using_profile_func(self):
        found = resolve("/")
        self.assertEqual(found.func, profile)